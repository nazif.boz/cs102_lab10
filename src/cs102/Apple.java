package cs102;

public class Apple  extends Fruit implements Peelable{

    public Apple(){
        this.setColor("Green");
    }

    @Override
    public String getVitamin() {
        return "A B12";
    }

    @Override
    public void Peelable() {
        System.out.println("Peeling an apple");
    }
}
