package cs102;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Fruit> fruits = new ArrayList<Fruit>();
        fruits.add(new Apple());
        fruits.add(new Banana());
        fruits.add(new Strawberry());
        fruits.add(new Blackberry());
        prepareFruits(fruits);
    }

    private static void prepareFruits(ArrayList<Fruit> fruits) {

        for (Fruit takenOne : fruits){
            if(takenOne instanceof Peelable){
                ((Peelable) takenOne).Peelable();
            }
           if (takenOne instanceof Pickable){
               ((Pickable) takenOne).Pickable();
           }


        }
    }

}
