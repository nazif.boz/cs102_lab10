package cs102;

public class Blackberry extends Fruit implements Pickable{

    public Blackberry(){
        this.setColor("Black");

    }
    @Override
    public String getVitamin() {
        return "C K";
    }

    @Override
    public void Pickable() {
        System.out.println("Picking a blackberry");
    }
}
