package cs102;

public class Banana extends  Fruit  implements Peelable{

    public Banana(){
        this.setColor("Yellow");
    }

    @Override
    public String getVitamin() {
        return "C D";
    }

    @Override
    public void Peelable() {
        System.out.println("Peeling a banana ");
    }
}
