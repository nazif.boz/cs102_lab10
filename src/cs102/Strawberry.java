package cs102;

public class Strawberry  extends Fruit implements Pickable {


    public Strawberry(){

        this.setColor("Red");

    }
    @Override
    public String getVitamin() {
        return "B5 A";
    }

    @Override
    public void Pickable() {
        System.out.println("Picking a strawberry");
    }
}

