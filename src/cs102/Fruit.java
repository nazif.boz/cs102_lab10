package cs102;

public abstract class Fruit {

    private String color;

    public abstract String getVitamin();

    public void setColor(String color){

        this.color=color;
    }

}
